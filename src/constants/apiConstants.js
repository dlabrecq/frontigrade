const API_ACCOUNT_ARN = 'account_arn';
const API_ACCOUNT_NAME = 'name';
const API_RESOURCE_TYPE = 'resourcetype';
const API_AWS_ACCOUNT_ID = 'aws_account_id';

const apiTypes = {
  API_ACCOUNT_ARN,
  API_ACCOUNT_NAME,
  API_RESOURCE_TYPE,
  API_AWS_ACCOUNT_ID
};

export { apiTypes as default, apiTypes, API_ACCOUNT_ARN, API_ACCOUNT_NAME, API_RESOURCE_TYPE, API_AWS_ACCOUNT_ID };
